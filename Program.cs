﻿using System;

class Program
{
    static void Main()
    {
        Console.OutputEncoding = System.Text.Encoding.UTF8;
        Console.WriteLine("Nhập chiều cao của tam giác: ");
        int chieuCao = Convert.ToInt32(Console.ReadLine());

        for (int i = 1; i <= chieuCao; i++)
        {
            for (int j = 1; j <= i; j++)
            {
                Console.Write("* ");
            }
            Console.WriteLine();
        }
    }
}
